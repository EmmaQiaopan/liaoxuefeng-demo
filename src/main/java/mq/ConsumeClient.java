package mq;

/**
 * 消费者-入口类
 *
 * @author gzh
 */
public class ConsumeClient {

  public static void main(String[] args) throws Exception {
    MqClient client = new MqClient();

    while (true){
      //消费数据
      String message = client.consume();
      System.out.println("获取的消息为：" + message);
      Thread.sleep(3000);
    }
  }
}

