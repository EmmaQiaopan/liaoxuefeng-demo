package mq;

/**
 * 生产者-入口类
 *
 * @author gzh
 */
public class ProduceClient {

  public static void main(String[] args) throws Exception {
    MqClient client = new MqClient();

    while (true){
      //发送消息
      client.produce("SEND:Hello World");
      Thread.sleep(3000);
    }
  }

}
