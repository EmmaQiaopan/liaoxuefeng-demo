package mq;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 存储节点：包含了消息队列
 *
 * @author gzh
 */
public class Broker {
  // 队列存储消息的最大数量
  private final static int MAX_SIZE = 3;

  /**
   * 消息队列的核心就是队列两个字，即核心数据结构就是阻塞队列
   */
  private static ArrayBlockingQueue<String> messageQueue = new ArrayBlockingQueue<String>(MAX_SIZE); // 保存消息数据的容器

  /**
   * 生产消息
   *
   * @param msg
   * @author gzh
   */
  public static void produce(String msg) {
    //写数据到消息队列
    if (messageQueue.offer(msg)) {
      System.out.println("成功向消息处理中心投递消息：" + msg + "，当前暂存的消息数量是：" + messageQueue.size());
    } else {
      System.out.println("消息处理中心内暂存的消息达到最大负荷，不能继续放入消息！");
    }
    System.out.println("=======================");
  }

  /**
   * 消费消息
   *
   * @return java.lang.String
   * @author gzh
   */
  public static String consume() {
    //从消息队列读数据
    String msg = messageQueue.poll();
    if (msg != null) {
      // 消费条件满足情况，从消息容器中取出一条消息
      System.out.println("已经消费消息：" + msg + "，当前暂存的消息数量是：" + messageQueue.size());
    } else {
      System.out.println("消息处理中心内没有消息可供消费！");
    }
    System.out.println("=======================");

    return msg;
  }

}

