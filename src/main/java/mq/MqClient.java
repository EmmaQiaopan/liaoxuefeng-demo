package mq;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * 访问消息队列的客户端：生产者和消费者都是基于mq客户端和mq服务器(即存储节点)通信
 */
public class MqClient {

  /**
   * 发送消息
   *
   * @param message
   * @author gzh
   */
  public static void produce(String message) throws Exception {
    //本地的的BrokerServer.SERVICE_PORT 创建SOCKET
    Socket socket = new Socket(InetAddress.getLocalHost(), BrokerServer.SERVICE_PORT); //创建套接字对象，写数据到存储节点
    try (
        PrintWriter out = new PrintWriter(socket.getOutputStream())
    ) {
      out.println(message);
      out.flush();
    }
  }

  /**
   * 消费消息
   *
   * @return java.lang.String
   * @author gzh
   */
  public static String consume() throws Exception {
    Socket socket = new Socket(InetAddress.getLocalHost(), BrokerServer.SERVICE_PORT); //创建套接字对象，从存储节点读数据
    try (
        BufferedReader in = new BufferedReader(new InputStreamReader(
            socket.getInputStream()));
        PrintWriter out = new PrintWriter(socket.getOutputStream())
    ) {
      //先向消息队列发送命令
      out.println("CONSUME");
      out.flush();

      //再从消息队列获取一条消息
      String message = in.readLine();

      return message;
    }
  }

}

