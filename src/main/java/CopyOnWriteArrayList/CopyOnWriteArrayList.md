# CopyOnWriteArrayList源码分析和实现原理

# 作用
写的同时，还可以读。

但是，只能一个写。

# 实现原理
## 写
不能并发写，即只能一个写。

## 读
可以并发读。

# 源码分析
## 写
有锁

## 读
无锁

# 并发map
ConcurrentHashMap
