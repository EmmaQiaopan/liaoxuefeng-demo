# AbstractQueuedSynchronizer源码分析和实现原理

# cas(compareAndSet)
## 作用
读写state字段，用于标记是否获取到锁：  
0就是没有获取到锁；  
1就是获取到锁。

## 实现原理
基于cas来修改state的值。

# AQS(AbstractQueuedSynchronizer)
## 作用
阻塞和唤醒线程：  
如果没有获取到锁，就阻塞当前线程；  
如果获取到锁，在释放锁之后，唤醒其他阻塞线程。

## 实现原理
unsafe.park/unpark方法。

# jdk源码调试

