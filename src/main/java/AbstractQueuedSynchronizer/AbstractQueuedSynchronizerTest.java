package AbstractQueuedSynchronizer;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author gzh
 * @createTime 2022/2/4 10:44 PM
 */
public class AbstractQueuedSynchronizerTest implements Runnable {
  ReentrantLock reentrantLock = new ReentrantLock();

  @Override
  public void run() {
    reentrantLock.tryLock();
    System.out.println("执行任务线程");
    reentrantLock.unlock();
  }

  public static void main(String[] args) {
    Thread thread1 = new Thread(new AbstractQueuedSynchronizerTest());
    thread1.start();

//    Thread thread2 = new Thread(new AbstractQueuedSynchronizer.AbstractQueuedSynchronizerTest());
//    thread2.start();
  }
}
