package ThreadPool;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务线程池
 *
 * ---
 * Learn Java from https://www.liaoxuefeng.com/
 * 
 * @author liaoxuefeng
 */
class ScheduledThreadPoolExecutorTest {
	public static void main(String[] args) {
		//创建线程池对象
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(4); //ScheduledThreadPoolExecutorTest

		// 开始执行定时任务，每3秒执行一次:
		ses.scheduleAtFixedRate(new Task2("fixed-rate"), 0, 3, TimeUnit.SECONDS);
	}
}

/**
 * 任务线程
 *
 * @author gzh
 * @date 2022/1/30 2:24 PM
 */
class Task2 implements Runnable {

	private final String name;

	public Task2(String name) {
		this.name = name;
	}

	@Override
	public void run() {
		System.out.println("start task " + new Date());
		try {
			Thread.sleep(1000); //
		} catch (InterruptedException e) {
		}
		System.out.println("end task " + new Date());
		System.out.println();
	}
}
