# ScheduledThreadPoolExecutor定时任务线程池
## 优点
解决Timer的缺点。

# Timer的缺点？
## 每个线程都要创建一个Timer对象

## 如果其中一个线程异常了，影响其他线程

# 总结
ScheduledThreadPoolExecutor定时任务线程池，就是用来替代Timer的。

jdk新版，基本上都不使用Timer了。
