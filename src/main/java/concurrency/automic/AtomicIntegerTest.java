package concurrency.automic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author gzh
 * @createTime 2022/1/20 9:56 AM
 */
public class AtomicIntegerTest {
  //全局唯一id
  static AtomicInteger atomicInteger = new AtomicInteger(0);

  public static void main(String[] args) {
    for (int i = 0; i < 100; i++) {
      //
      Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
          //每个线程的唯一id都自增
//          int i = incrementAndGet(atomicInteger); //调用自己实现的方法
          int i = atomicInteger.incrementAndGet(); //调用官方的方法
          System.out.println(i);
        }
      });

      //
      thread.start();
    }
  }

  /**
   * 自己实现incrementAndGet方法：即当前值加1（线程安全）
   *
   * @author gzh
   * @date 2022/1/20 9:56 AM
   * @param var 当前值
   * @return int 当前值加1
   */
  public static int incrementAndGet(AtomicInteger var) {
    int expect, next;
    do {
      expect = var.get();
      next = expect + 1;
    } while ( ! var.compareAndSet(expect, next)); //1、如果当前值等于期待的值，那么返回true 2、如果不等于，那么返回false

    return expect; //当前值自增1(因为当前值，已经被设置为cas方法的第二个入参uodate的值了)
  }

}
