package concurrency.forkjoin;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

/**
 * Learn Java from https://www.liaoxuefeng.com/
 * 
 * @author liaoxuefeng
 */
public class ForkJoinTest {
	public static void main(String[] args) throws Exception {
		// 创建2000个随机数 组成的数组:
		long[] array = new long[2000];
		long expectedSum = 0;
		for (int i = 0; i < array.length; i++) {
			array[i] = random();
			expectedSum += array[i]; //计算所有数据的和
		}
		System.out.println("Expected sum: " + expectedSum); //打印和
		System.out.println();

		//创建任务线程对象
		ForkJoinTask<Long> task = new SumTask(array, 0, array.length);
		long startTime = System.currentTimeMillis();
		//执行任务线程
		Long result = ForkJoinPool.commonPool().invoke(task);
		long endTime = System.currentTimeMillis();

		//打印耗时
		System.out.println();
		System.out.println("Fork/join sum: " + result + " in " + (endTime - startTime) + " ms."); //打印和、耗时
	}

	static Random random = new Random(0);

	/**
	 * 获取随机数
	 *
	 * @author gzh
	 * @date 2022/1/20 10:33 AM
	 * @return long
	 */
	static long random() {
		return random.nextInt(10000);
	}
}

/**
 * 内部类：计算和
 *
 * @author gzh
 * @date 2022/1/20 10:23 AM
 */
class SumTask extends RecursiveTask<Long> {

	static final int THRESHOLD = 500; //阈值
	long[] array;
	int start;
	int end;

	SumTask(long[] array, int start, int end) {
		this.array = array;
		this.start = start;
		this.end = end;
	}

	@Override
	protected Long compute() {
		if (end - start <= THRESHOLD) { //如果小于阈值500，就直接计算
			// 如果任务足够小,直接计算:
			long sum = 0;
			for (int i = start; i < end; i++) {
				sum += this.array[i];
				// 故意放慢计算速度:
				try {
					Thread.sleep(2);
				} catch (InterruptedException e) {
				}
			}
			return sum;
		}

		// 任务太大,一分为二:
		int middle = (end + start) / 2; //如果大于阈值500，就拆分
		System.out.println(String.format("split %d~%d ==> %d~%d, %d~%d", start, end, start, middle, middle, end));
		SumTask subtask1 = new SumTask(this.array, start, middle); //子任务线程1
		SumTask subtask2 = new SumTask(this.array, middle, end);   //子任务线程2
		invokeAll(subtask1, subtask2); //执行任务线程
		Long subresult1 = subtask1.join(); //获取子任务线程1的计算结果
		Long subresult2 = subtask2.join(); //获取子任务线程2的计算结果
		Long result = subresult1 + subresult2; //计算拆分的和
		System.out.println("result = " + subresult1 + " + " + subresult2 + " ==> " + result);
		return result;
	}
}
