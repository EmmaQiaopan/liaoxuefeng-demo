package concurrency.forkjoin;

/**
 * 菲波齐纳数列的每个值，都是前面两个值的和
 *
 * ---
 * 参考：维基百科
 *
 * @author gzh
 * @createTime 2022/1/20 10:58 AM
 */
public class FibonacciTest2 {
  static int i = 6; //第几个数？

  public static void main(String[] args) {
    //
    int value = fibonacci(i); //第三个数2 = 第二个数1 + 第一个数1
//    int i2 = fibonacci(5); // 1 1 2 3 5  //第五个数5 = 第四个数3 + 第三个数2

    //
    System.out.println(value);
//    System.out.println(i2);
  }

  /**
   * 递归计算
   *
   * @author gzh
   * @date 2022/1/20 10:58 AM
   * @param i 第几个数？
   * @return int 第几个数的值是啥？
   */
  public static int fibonacci(int i){
    System.out.println("当前是第几个数：" + i);
    if(i<2){
      return i;
    }else {
      return fibonacci(i-1)+fibonacci(i-2); //递归调用
    }
  }
}
