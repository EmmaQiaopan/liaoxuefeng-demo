package concurrency.forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 * 演示斐波那契数列
 *
 * ---
 * 参考：RecursiveTask官方api的demo
 *
 * @author gzh
 * @date 2022/1/20 10:45 AM
 */
class Fibonacci {
  static int i = 6; //第几个数？

  public static void main(String[] args) {
    //创建任务线程
    FibonacciTask fibonacciTask = new FibonacciTask(i);
    //执行任务线程
    Integer value = ForkJoinPool.commonPool().invoke(fibonacciTask);

    //打印返回结果
    System.out.println();
    System.out.println(value);
  }

}


/**
 * @author gzh
 * @createTime 2022/1/20 10:37 AM
 */
class FibonacciTask extends RecursiveTask<Integer> { //要继承递归任务
  final int i; //第几个数？

  FibonacciTask(int i) {
    this.i = i;
  }

  public Integer compute() { //执行任务，并且返回结果——相当于1.先调用fork方法执行任务 2.然后调用join方法获取执行结果
    if (i <= 1)
      return i;

    FibonacciTask f1 = new FibonacciTask(i - 1); //创建任务线程1
//    f1.fork(); //执行任务线程1
    FibonacciTask f2 = new FibonacciTask(i - 2); //创建任务线程2
    return f2.compute() + f1.compute(); //两个任务线程的结果相加  // 1 1 2 3 5  //第五个数5 = 第四个数3 + 第三个数2
  }
}
