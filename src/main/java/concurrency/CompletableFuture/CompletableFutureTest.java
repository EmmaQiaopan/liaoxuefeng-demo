package concurrency.CompletableFuture;

import java.util.concurrent.CompletableFuture;

/**
 * Learn Java from https://www.liaoxuefeng.com/
 * 
 * @author liaoxuefeng
 */
public class CompletableFutureTest {
	public static void main(String[] args) throws Exception {
		// 两个CompletableFuture执行异步查询:
		CompletableFuture<String> cfQueryFromSina = CompletableFuture.supplyAsync(() -> { //两个异步查询
			return queryCode("中国石油", "https://finance.sina.com.cn/code/");
		});
		CompletableFuture<String> cfQueryFrom163 = CompletableFuture.supplyAsync(() -> {
			return queryCode("中国石油", "https://money.163.com/code/");
		});

		// 用anyOf合并为一个新的CompletableFuture:
		CompletableFuture<Object> cfQuery = CompletableFuture.anyOf(cfQueryFromSina, cfQueryFrom163); //只要有一个成功，就立马返回——如果用Callable.get方法，就只能是阻塞，两个查询要按顺序。之前工作就是使用Callable，其实是可以优化的。



		// 两个CompletableFuture执行异步查询:
		CompletableFuture<Double> cfFetchFromSina = cfQuery.thenApplyAsync((code) -> { //基于上面的查询结果，再次继续两个异步查询
			return fetchPrice((String) code, "https://finance.sina.com.cn/price/");
		});
		CompletableFuture<Double> cfFetchFrom163 = cfQuery.thenApplyAsync((code) -> {
			return fetchPrice((String) code, "https://money.163.com/price/");
		});

		// 用anyOf合并为一个新的CompletableFuture:
		CompletableFuture<Object> cfFetch = CompletableFuture.anyOf(cfFetchFromSina, cfFetchFrom163); //只要有一个成功，就立马返回



		// 最终结果:
		cfFetch.thenAccept((result) -> {
			System.out.println("price: " + result);
		});



		// 主线程不要立刻结束，否则CompletableFuture默认使用的线程池会立刻关闭:
		Thread.sleep(2000);
	}

	/**
	 * 查询股票代码
	 *
	 * @author gzh
	 * @date 2022/1/18 10:01 AM
	 * @param name
	 * @param url
	 * @return java.lang.String
	 */
	static String queryCode(String name, String url) {
		System.out.println("query code from " + url + "...");
		try {
			Thread.sleep((long) Math.random() * 1000);
		} catch (InterruptedException e) {
		}
		return "601857";
	}

	/**
	 * 根据股票代码，查询股票价格
	 *
	 * @author gzh
	 * @date 2022/1/18 10:01 AM
	 * @param code
	 * @param url
	 * @return java.lang.Double
	 */
	static Double fetchPrice(String code, String url) {
		System.out.println("query price from " + url + "...");
		try {
			Thread.sleep((long) Math.random() * 1000);
		} catch (InterruptedException e) {
		}
		return 5 + Math.random() * 20;
	}
}
