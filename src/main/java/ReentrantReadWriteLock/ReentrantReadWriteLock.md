# ReentrantReadWriteLock源码分析和实现原理

# 作用
## 只能一个写，并且写的时候不能读
这是读写锁，和并发集合/并发map最大的区别——并发集合/并发map，写的时候，是可以读的。


# state字段的值
## 读锁
第一次获取的时候，state值是65536。
为什么是65536？因为读锁是高16位，10000000000000000(65536)取高16位的值是1，所以其实还是用0/1来表示是否有锁。

释放的时候，也是减去65536，然后state就被更新为0。

## 写锁

# 源码分析
## 获取锁

## 释放锁

# 实现原理

# 写锁和读锁，是如何表示的？
## 如何表示？
state的低16位表示写锁；  
高16位表示读锁。

## 锁的数量
写锁的数量：只能是同一个线程重入多次。

读锁的数量：有可能是同一个线程重入多次，也可能是多个不同线程都获取到了读锁——因为读锁允许多个线程同时访问。

# 参考
http://generalthink.github.io/2020/12/16/rrw-analysis/

