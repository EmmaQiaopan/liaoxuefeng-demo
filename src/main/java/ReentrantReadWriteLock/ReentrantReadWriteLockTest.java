package ReentrantReadWriteLock;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author gzh
 * @createTime 2022/2/5 3:52 PM
 */
public class ReentrantReadWriteLockTest implements Runnable {
  ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

  @Override
  public void run() {
    readWriteLock.readLock().lock();
    System.out.println("执行任务线程");
    readWriteLock.readLock().unlock();
  }

  public static void main(String[] args) {
    Thread thread1 = new Thread(new ReentrantReadWriteLockTest());
    thread1.start();

//    Thread thread2 = new Thread(new AbstractQueuedSynchronizer.AbstractQueuedSynchronizerTest());
//    thread2.start();
  }
}