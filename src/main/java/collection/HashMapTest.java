package collection;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gzh
 * @createTime 2022/2/7 10:59 AM
 */
public class HashMapTest {

  public static void main(String[] args) {
    int i = 2;
    i = i>>>1;
    System.out.println(i);

    //
    Map map = new HashMap<>();
    int h = hash("name1");
    System.out.println(h % 16); //取模：13
    System.out.println((16 - 1) & h); //按位与：13
    for (int j = 1; j <= 3; j++) {
      map.put("name" + j, "gzh" + j);
    }
//    map.put("name1","gzh1");
//    map.put("name1","gzh2");

    map.get("name1");
  }

  static final int hash(Object key) {
    System.out.println(key.hashCode());
    System.out.println(key.hashCode() >>> 16);

    int h;
    h = (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16); //hashcode和右移16位：按位异或
    System.out.println(h);
    return h;
  }
}
