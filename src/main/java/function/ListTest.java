package function;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author gzh
 */
public class ListTest {
  //遍历
//  public static void main(String[] args) {
//    List list = new ArrayList();
//    list.add("gzh1");
//    list.add("gzh2");
//    list.add("gzh3");
//
//    for (int i = 0; i < list.size(); i++) {
//      System.out.println(list.get(i));
//    }
//  }

  //迭代器
//  public static void main(String[] args) {
//    List list = new ArrayList();
//    list.add("gzh1");
//    list.add("gzh2");
//    list.add("gzh3");
//
//    Iterator<String> itr = list.iterator();
//    while (itr.hasNext()) {
//      System.out.println(itr.next());
//    }
//  }

    //jdk8新增的foreach方法
  public static void main(String[] args) {
    List list = new ArrayList();
    list.add("gzh1");
    list.add("gzh2");
    list.add("gzh3");

    list.forEach(x -> System.out.println(x));
  }
}
