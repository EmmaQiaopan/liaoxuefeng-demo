package function;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;

/**
 * @author gzh
 */
@Slf4j
public class MapTest {
//  public static void main(String[] args) {
//    String str = "hello java, i am vary happy! nice to meet you";
//
//    // jdk1.8之前的写法
//    HashMap<Character, Integer> result1 = new HashMap<>(32);
//    //遍历每个字符
//    for (int i = 0; i < str.length(); i++) {
//      char curChar = str.charAt(i);
//      Integer curVal = result1.get(curChar);
//      if (curVal == null) {
//        curVal = 1;
//      } else {
//        curVal += 1;
//      }
//      result1.put(curChar, curVal);
//    }
//
//    log.info("result1:{}",JSON.toJSONString(result1));
//  }

  public static void main(String[] args) {
    String str = "hello java, i am vary happy! nice to meet you";

    // jdk1.8的写法
    HashMap<Character, Integer> result1 = new HashMap<>(32);
    for (int i = 0; i < str.length(); i++) {
      char curChar = str.charAt(i);
      result1.compute(curChar, (k, v) -> { //
        if (v == null) {
          v = 1;
        } else {
          v += 1;
        }
        return v;
      });
    }

    log.info("result1:{}",JSON.toJSONString(result1));
  }


}
